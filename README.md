# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


# Instalando el proyecto
Se debe clonar el repositorio y ejecutar `npm install` para instalar todos los paquetes necesarios.
Una vez hecho esto simplemente ejecute `npm start` para levantar la aplicacion.

La respuesta acá mostrada consta de una única pantalla en la se muestra inicialmente un formulario de login, de ser satisfactorio dicho login se muestra la información
requerida de acuerdo a lo estipulado en la prueba

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

