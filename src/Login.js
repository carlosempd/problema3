import React, { useState } from 'react'
import { BookingGrid } from './BookingGrid';
import { useForm } from './hooks/useForm';

import logo from './logo.svg';

export const Login = () => {
    
    const url = 'https://dev.tuten.cl/TutenREST/rest/user';
    
    const [isLogged, setIsLogged] = useState(false);

    const [error, setError] = useState(false);

    const [dataBooking, setDataBooking] = useState([]);

    const [ formValues, handleInputChange ] = useForm({
        email: '',
        password: ''
    });

    const { email, password } = formValues;

    /**
     * Handle Submit of the form
     * @param {*} e evento submit 
     */
    const handleSubmit = ( e ) => {
        e.preventDefault();
        
        handleLogin();
    };
    
    /**
     * Manage all the logic to perform the login
     */
    const handleLogin = async() => {

        const urlLogin = `${ url }/${ email }`;


        const resp = await fetch( urlLogin, {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                password,
                app: 'APP_BCK' 
            },
        });

        const data = await resp.json().catch( () => { setError(true) });


        if ( data ) {
            setError( false );
            setIsLogged( true );

            getBookingInfo( data.sessionTokenBck );
        };
        

    }

    /**
     * Gets the info of the booking once the user is logged in
     * 
     * @param {*} token access token once logged in 
     */
    const getBookingInfo = async( token ) => {
        
        const urlBooking = `${ url }/contacto@tuten.cl/bookings?current=true`;

        const respBooking = await fetch( urlBooking,  {
            method: 'GET',
            headers: {
                adminemail: email,
                Accept: 'application/json',
                token,
                app: 'APP_BCK'
            }
        });

        const data = await respBooking.json().catch( () => { console.log('ERROR') });
        if ( data ) setDataBooking( data );

    }



    return (
        <>  
            
            <h2>
                Login
                <img src={logo} className="App-logo" alt="logo" width="50"/>
            </h2> 
 

            <hr />

            <form onSubmit={ handleSubmit }>
                <div className="mb-3">
                    <label for="exampleInputEmail1" className="form-label">
                        Email address
                    </label>

                    <input 
                        type="email" 
                        className="form-control" 
                        id="email"
                        name="email"
                        value={ email }
                        autoComplete="off"
                        onChange={ handleInputChange } 
                        aria-describedby="emailHelp" 
                    />
                    <div id="emailHelp" className="form-text">Please enter your email.</div>

                </div>

                <div class="mb-3">
                    <label for="exampleInputPassword1" className="form-label">
                        Password
                    </label>
                    <input 
                        type="password" 
                        className="form-control" 
                        id="password"
                        name="password"
                        onChange={ handleInputChange }
                        value={ password }
                    />
                </div>

                <button type="submit" className="btn btn-primary">Submit</button>
            </form>

            { 
                error 
                
                &&

                <div className="alert alert-warning mt-4" role="alert">
                    An error has ocurred, please verify email/password
                </div>

            }

            {
                (!error && isLogged)

                &&

                <BookingGrid dataBooking={ dataBooking }/>
            }

            

        </>
    )
}
