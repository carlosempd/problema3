import React from 'react'



export const BookingGrid = ({ dataBooking }) => {

    return (
        <div className="mt-5">
          <h3>Booking Grid Info</h3>  
          <hr />

          <table class="table">
                <thead className="table-dark">
                    <tr>
                    <th scope="col">BookingId</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Fecha de creación</th>
                    <th scope="col">Dirección</th>
                    <th scope="col">Precio</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        dataBooking.map( ({ bookingId, tutenUserClient, bookingTime, locationId, bookingPrice}) => (
                            <tr key={ bookingId }>
                                <th scope="row">{ bookingId }</th>
                                <td>{ `${ tutenUserClient.firstName } ${ tutenUserClient.lastName }` }</td>
                                <td>
                                    { `${ (new Date( bookingTime )).getDay() }-${ (new Date( bookingTime )).getMonth() }-${ (new Date( bookingTime )).getFullYear() }` }
                                </td>
                                <td>{ locationId.streetAddress }</td>
                                <td>{ bookingPrice }</td>
                            </tr>

                        ))
                    }


                </tbody>
            </table>
        </div>
    )
}
